package main

import (
	"fmt"
	"sync"
	"time"
)

var shareCh = make(chan string, 2)
var statusCh = make(chan StatusManager, 2)
var wg sync.WaitGroup

var (
	TaskAType  	=  "A"
	TaskBType  	= "B"
	TaskAName    = "Task-A"
	TaskBName    = "Task-B"
	PauseCommand = "PAUSE"
	StopCommand  = "STOP"
)

var (
	secondsOfVN         = int((7 * time.Hour).Seconds())
	VNLocation          = time.FixedZone("Vietnam", secondsOfVN)
	FormatYYYMMDDHHMMSS = "2006-01-02 15:04:05"
)

func getVietnamCurrentTimeInString() string {
	t := time.Now().In(VNLocation)
	return t.Format(FormatYYYMMDDHHMMSS)
}

func main() {
	wg.Add(2)

	taskFactory := &TaskFactory{}
	taskA := taskFactory.NewTask(TaskAType, TaskAName)
	taskB := taskFactory.NewTask(TaskBType, TaskBName)

	var tasks []Task
	tasks = append(tasks, taskA, taskB)

	taskManager := NewTaskManager()
	taskManager.Add(tasks)
	taskManager.Start()

	time.Sleep(5 * time.Second)
	taskManager.Pause()

	time.Sleep(2 * time.Second)
	taskManager.Stop()

	time.Sleep(2 * time.Second)
	taskManager.Remove()

	wg.Wait()
	fmt.Println("\n=============== Task Manager HAS DONE!!! =============== ")
}
