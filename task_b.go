package main

import (
	"time"
)

type TaskB struct {
	BaseTask
	Data string
}

var _ Task = (*TaskB)(nil)

func (t *TaskB) DoJob() {
	defer wg.Done()

	ticker := time.NewTicker(1 * time.Second)
	for {
		select {
		case <-ticker.C:
			println("\nI'm Task-B, I run each 1 seconds and share data to Task-A")
			data := getVietnamCurrentTimeInString()
			println("My data is: ", data)
			shareCh <- data
		case cmd := <-statusCh:
			if cmd.TaskName == t.Name {
				switch cmd.Status {
				case PauseCommand:
					println("I'm Task-B, I will pause")
					ticker.Stop()
				case StopCommand:
					println("I'm Task-B, I will stop running")
					wg.Done()
					return
				default:
					println("I'm Task-B, I'am running")
				}
			}
		}
	}
}

func (t *TaskB) GetData() string {
	return t.Data
}

func (t *TaskB) GetName() string {
	return t.BaseTask.Name
}
