package main

type Task interface {
	DoJob()
	GetData() string
	GetName() string
}

type BaseTask struct {
	Name string
}

type TaskManager struct {
	TaskList []Task
}

type StatusManager struct {
	TaskName string
	Status   string
}

type TaskFactory struct {}

func (f *TaskFactory) NewTask(taskType string, name string) Task {
	switch taskType {
	case TaskAType:
		return &TaskA{
			BaseTask: BaseTask{
				Name: name,
			},
		}
	case TaskBType:
		return &TaskB{
			BaseTask: BaseTask{
				Name: name,
			},
		}

	default:
		return nil
	}
}
