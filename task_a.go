package main

import (
	"time"
)

type TaskA struct {
	BaseTask
}

var _ Task = (*TaskA)(nil)

func (t *TaskA) DoJob() {
	defer wg.Done()

	ticker := time.NewTicker(3 * time.Second)
	for {
		select {
		case <-ticker.C:
			println("\nI'm Task-A, I run each 3 seconds")
			println("And I got data from Task-B: ", <-shareCh)
		case cmd := <-statusCh:
			if cmd.TaskName == t.Name {
				switch cmd.Status {
				case PauseCommand:
					println("I'm Task-A, I will pause")
					ticker.Stop()
				case StopCommand:
					println("I'm Task-A, I will stop running")
					return
				default:
					println("I'm Task-A, I'm running")
				}
			}
		}
	}
}

func (t *TaskA) GetData() string {
	return ""
}

func (t *TaskA) GetName() string {
	return t.BaseTask.Name
}
