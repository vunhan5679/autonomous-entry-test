package main

func NewTaskManager() *TaskManager {
	return &TaskManager{TaskList: nil}
}

func (t *TaskManager) SetTaskList(tasks []Task) {
	if tasks == nil {
		t.TaskList = nil
		return
	}

	t.TaskList = append(t.TaskList, tasks...)
}

func (t *TaskManager) GetTaskList() []Task {
	return t.TaskList
}

func (t *TaskManager) Add(tasks []Task) {
	println("\n=============== Task Manager ADDS tasks =============== ")
	t.SetTaskList(tasks)
}

func (t *TaskManager) Start() {
	println("\n=============== Task Manager STARTS all tasks =============== ")
	for _, task := range t.GetTaskList() {
		go task.DoJob()
	}
}

func (t *TaskManager) Stop() {
	println("\n=============== Task Manager STOPS all tasks =============== ")

	for _, task := range t.GetTaskList() {
		statusCh <- StatusManager{
			TaskName: task.GetName(),
			Status:   StopCommand,
		}
	}
}

func (t *TaskManager) Pause() {
	println("\n=============== Task Manager PAUSES all tasks =============== ")
	for _, task := range t.GetTaskList() {
		statusCh <- StatusManager{
			TaskName: task.GetName(),
			Status:   PauseCommand,
		}
	}
}

func (t *TaskManager) Remove() {
	println("\n=============== Task Manager REMOVES tasks =============== ")
	t.SetTaskList(nil)
}
