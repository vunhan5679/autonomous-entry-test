### Project Description

main.go => Call functions .

task.go => Define Task interface .

task_a.go => Define TaskA and implement functions .

task_b.go => Define TaskB and implement functions .

task_manager.go => Define Task Manager and implement functions .


### Business Description

Firstly, Task Manager add all tasks and start them then.

After 5 seconds, Task Manager will Pause all tasks.

After 5 seconds, Task Manager will Stop all tasks and remove all them.

Export, Import functions haven't yet implemented.